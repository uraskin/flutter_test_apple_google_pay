import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:stripe_native/stripe_native.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
 
  @override
  void initState() {
    super.initState();
    StripeNative.setPublishableKey("pk_test_51H0SYdFbCcXBHYnZAqAdrOGTXzfsI4RQcmMsjXPfgzaFJ1LkERL7FG79F1uEWFLj5mXKyt3T8s14Ps7rZ8VaDQ7K00qzDkubfi");
    StripeNative.setMerchantIdentifier("merchant.com.uraskin.stripenative.test");
  }

  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(child: nativeButton)
      
    );
  }

  Future<String> get receiptPayment async {
    /* custom receipt w/ useReceiptNativePay */
    const receipt = <String, double>{"Nice Hat": 1.00, "Used Hat" : 0.50};
    var aReceipt = Receipt(receipt, "Hat Store");
    return await StripeNative.useReceiptNativePay(aReceipt);
  }

  Future<String> get orderPayment async {
    // subtotal, tax, tip, merchant name
    var order = Order(5.50, 1.0, 2.0, "Some Store");
    return await StripeNative.useNativePay(order);
  }

  Widget get nativeButton => Padding(padding: EdgeInsets.all(10), child: RaisedButton(padding: EdgeInsets.all(10),
      child: Text("Native Pay"),
      onPressed: () async {
        if (Platform.isIOS) {
          final applePayIsSupported = await _checkAppleLayIsSupported();
          if (!applePayIsSupported) {
            print('Apple Pay not supported');
            return;
          }
        }
        var token = await receiptPayment;
        print(token);
        StripeNative.confirmPayment(true); // iOS load to check.
      }
    ));

  Future<bool> _checkAppleLayIsSupported() async {
    const platform = const MethodChannel('stripe_native_test');
    return await platform.invokeMethod('checkSupportApplePay');
  }
}
